# First Lab #

### Tasks ###

1. [x] Установить все рассмотренные инструменты: [JDK 8](https://www.oracle.com/java/technologies/javase/javase8u211-later-archive-downloads.html), [Maven](https://maven.apache.org), [Git](https://git-scm.com/), [IntelliJ IDEA Ult/Com](https://www.jetbrains.com/idea/);
2. [x] Зарегистрироваться на [Bitbucket](https://bitbucket.org);
3. [x] Выполнить компиляцию и запуск класса java из коммандной строки по примеру, приведённому в лекции;
4. [x] Создать репозиторий/ии для выполнения домашних работ и дать доступ к репозиторию лектору.
5. [x] Пройти [Learn Git Branching](https://learngitbranching.js.org/)
6. [x] Ознакомиться с [Git How To](https://githowto.com/ru)